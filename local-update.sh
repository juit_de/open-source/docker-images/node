#!/usr/bin/env bash

if [[ ! -e "/.dockerenv" ]]; then
	docker run \
		--rm \
		-it \
		-v $(PWD):$(PWD) \
		-w $(PWD) \
		debian:buster \
		./local-update.sh
	exit
fi

apt-get update -y
apt-get install -yqqf --no-install-recommends --fix-missing wget gawk curl jq openssh-client git sshpass ca-certificates

./update.sh
